FROM nginx:alpine

COPY css/style.css /usr/share/nginx/html/css/
COPY img/logo.png /usr/share/nginx/html/img/
COPY js/main.js /usr/share/nginx/html/js/
COPY index.html /usr/share/nginx/html/


